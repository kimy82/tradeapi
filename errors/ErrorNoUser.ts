export default class ErrorNoUser extends Error {
    constructor(message) {
        super(message);
    }
}