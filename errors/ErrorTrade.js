"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorTrade extends Error {
    constructor(message) {
        super(message);
    }
}
exports.default = ErrorTrade;
