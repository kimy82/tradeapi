export default class ErrorTrade extends Error {
    constructor(message) {
        super(message);
    }
}