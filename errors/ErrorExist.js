"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorExists extends Error {
    constructor(message) {
        super(message);
    }
}
exports.default = ErrorExists;
