export default class ErrorExists extends Error {
    constructor(message) {
        super(message);
    }
}