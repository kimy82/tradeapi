"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sqlite3 = require("sqlite3");
const debug = require("debug");
const serverDebug = debug('StockDatasetAPI:server');
class DbService {
}
DbService.init = () => __awaiter(this, void 0, void 0, function* () {
    const dbVerbose = sqlite3.verbose();
    DbService.db = new dbVerbose.Database(':memory:');
    yield DbService.createTradeTable();
    //DbService.db.close(); TOD=: Close on SIGINT
    return;
});
DbService.deleteTrades = () => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        DbService.db.run('DELETE FROM trade', (err) => {
            if (err) {
                serverDebug('Could not delete trade rows ', err);
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
});
DbService.insertTrade = (trade) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            var stmtUser = DbService.db.prepare("INSERT INTO user VALUES (?, ?)");
            stmtUser.run([trade.user.id, trade.user.name]);
            stmtUser.finalize();
            var stmtTrade = DbService.db.prepare("INSERT INTO trade VALUES (?, ?, ?, ?, ?, ?, ?)");
            stmtTrade.run([trade.id, trade.type, trade.symbol, trade.shares, trade.price.toFixed(2), new Date(), trade.user.id]);
            stmtTrade.finalize();
            resolve();
        }
        catch (e) {
            reject(e);
        }
    }));
});
DbService.getTrades = () => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            DbService.db.all('Select id, type, symbol, shares, price, timestamp, user_id from trade order by id', (err, rows) => __awaiter(this, void 0, void 0, function* () {
                if (err) {
                    reject(err);
                }
                else {
                    const trades = yield DbService.processTradeDB(rows);
                    resolve(trades);
                }
            }));
        }
        catch (e) {
            reject(e);
        }
    }));
});
DbService.findByUserId = (id) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            DbService.db.all('Select id, type, symbol, shares, price, timestamp, user_id from trade  where user_id = ? order by id', [id], (err, rows) => __awaiter(this, void 0, void 0, function* () {
                if (err) {
                    reject(err);
                }
                else {
                    const trades = yield DbService.processTradeDB(rows);
                    resolve(trades);
                }
            }));
        }
        catch (e) {
            reject(e);
        }
    }));
});
DbService.existTradeById = (id) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        DbService.db.get('Select id, type, symbol, shares, price, timestamp, user_id from trade where id = ?', [id], (err, trade) => {
            if (err) {
                serverDebug(`Trade error ${id}`, err);
                reject(err);
            }
            else {
                resolve(trade ? true : false);
            }
        });
    });
});
DbService.existUserById = (id) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        DbService.db.get('Select id, name from user where id = ?', [id], (err, user) => {
            if (err) {
                serverDebug(`No user for the trade ${id}`, err);
                reject(err);
            }
            else {
                resolve(user ? true : false);
            }
        });
    });
});
DbService.processTradeDB = (tradesDb) => __awaiter(this, void 0, void 0, function* () {
    const trades = [];
    for (const trade of tradesDb) {
        try {
            const buildTrade = yield DbService.addUserToTrade(trade);
            trades.push(buildTrade);
        }
        catch (e) {
            serverDebug('Failed to retrieve User', e);
        }
    }
    return trades;
});
DbService.addUserToTrade = (trade) => __awaiter(this, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        DbService.db.get('Select id, name from user where id = ?', [trade.user_id], (err, user) => {
            console.log(user);
            if (err) {
                serverDebug(`No user for the trade ${trade.id}`, err);
                reject(err);
            }
            else {
                resolve({ id: trade.id, symbol: trade.symbol, shares: trade.shares, type: trade.type, price: trade.price, timestamp: trade.timestamp, user: user });
            }
        });
    });
});
DbService.createTradeTable = () => {
    return new Promise((resolve, reject) => {
        DbService.db.serialize(() => {
            try {
                DbService.db.run("CREATE TABLE user (id INT, name TEXT)");
                DbService.db.run("CREATE TABLE trade (id INT, type TEXT, symbol TEXT, shares INT, price FLOAT, timestamp TIMESTAMP, user_id INTEGER NOT NULL, FOREIGN KEY (user_id) REFERENCES user(id))");
                serverDebug('User & Trade Tables created');
                resolve();
            }
            catch (e) {
                reject(e);
            }
        });
    });
};
exports.DbService = DbService;
