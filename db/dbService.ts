import * as sqlite3 from 'sqlite3';
import * as debug from 'debug';
const serverDebug = debug('StockDatasetAPI:server');

export class DbService {

    private static db: sqlite3.Database;

    public static init = async (): Promise<void> => {
        const dbVerbose = sqlite3.verbose();
        DbService.db = new dbVerbose.Database(':memory:');
        await DbService.createTradeTable();
        //DbService.db.close(); TOD=: Close on SIGINT
        return;
    }

    public static deleteTrades = async (): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            DbService.db.run('DELETE FROM trade', (err) => {
                if (err) {
                    serverDebug('Could not delete trade rows ', err);
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    public static insertTrade = async (trade: ITrade): Promise<void> => {
        return new Promise<void>(async (resolve, reject) => {
            try {
                var stmtUser = DbService.db.prepare("INSERT INTO user VALUES (?, ?)");
                stmtUser.run([trade.user.id, trade.user.name]);
                stmtUser.finalize();
                var stmtTrade = DbService.db.prepare("INSERT INTO trade VALUES (?, ?, ?, ?, ?, ?, ?)");
                stmtTrade.run([trade.id, trade.type, trade.symbol, trade.shares, trade.price.toFixed(2), new Date(), trade.user.id]);
                stmtTrade.finalize();
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    public static getTrades = async (): Promise<ITrade[]> => {
        return new Promise<ITrade[]>(async (resolve, reject) => {
            try {
                DbService.db.all('Select id, type, symbol, shares, price, timestamp, user_id from trade order by id', async (err, rows) => {
                    if (err) {
                        reject(err);
                    } else {
                        const trades: ITrade[] = await DbService.processTradeDB(rows);
                        resolve(trades);
                    }
                })
            } catch (e) {
                reject(e);
            }
        });
    }
   
    public static findByUserId = async (id: number): Promise<ITrade[]> => {
        return new Promise<ITrade[]>(async (resolve, reject) => {
            try {
                DbService.db.all('Select id, type, symbol, shares, price, timestamp, user_id from trade  where user_id = ? order by id', [id], async (err, rows) => {
                    if (err) {
                        reject(err);
                    } else {
                        const trades: ITrade[] = await DbService.processTradeDB(rows);
                        resolve(trades);
                    }
                })
            } catch (e) {
                reject(e);
            }
        });
    }
    
    public static existTradeById = async (id: number): Promise<boolean> => {
        return new Promise<boolean> ((resolve, reject) => {
            DbService.db.get('Select id, type, symbol, shares, price, timestamp, user_id from trade where id = ?', [id], (err, trade) => {
                if (err) {
                    serverDebug(`Trade error ${id}`, err);
                    reject(err);
                } else {
                    resolve( trade ? true : false);
                }
            })
        })
    }

    public static existUserById = async (id: number): Promise<boolean> => {
        return new Promise<boolean> ((resolve, reject) => {
            DbService.db.get('Select id, name from user where id = ?', [id], (err, user) => {
                if (err) {
                    serverDebug(`No user for the trade ${id}`, err);
                    reject(err);
                } else {
                    resolve( user ? true : false);
                }
            })
        })
    }

    
    private static processTradeDB = async (tradesDb: ITradeDB[]): Promise<ITrade[]> => {
        const trades: ITrade[] = [];
        for (const trade of tradesDb) {
            try {
                const buildTrade = await DbService.addUserToTrade(trade);
                trades.push(buildTrade);
            } catch(e) {
                serverDebug('Failed to retrieve User', e);
            }
        }
        return trades;
    }

    private static addUserToTrade = async (trade: ITradeDB): Promise<ITrade> => {
        return new Promise<ITrade> ((resolve, reject) => {
            DbService.db.get('Select id, name from user where id = ?', [trade.user_id], (err, user) => {
                console.log(user)
                if (err) {
                    serverDebug(`No user for the trade ${trade.id}`, err);
                    reject(err);
                } else {
                    resolve({ id: trade.id, symbol: trade.symbol, shares: trade.shares, type: trade.type, price: trade.price, timestamp: trade.timestamp, user: user });
                }
            })
        })
    }

    private static createTradeTable = (): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            DbService.db.serialize(() => {
                try {
                    DbService.db.run("CREATE TABLE user (id INT, name TEXT)");
                    DbService.db.run("CREATE TABLE trade (id INT, type TEXT, symbol TEXT, shares INT, price FLOAT, timestamp TIMESTAMP, user_id INTEGER NOT NULL, FOREIGN KEY (user_id) REFERENCES user(id))");
                    serverDebug('User & Trade Tables created')
                    resolve();
                } catch (e) {
                    reject(e);
                }
            });
        });
    }
}