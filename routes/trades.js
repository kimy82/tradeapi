"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const trades_1 = require("../controllers/trades");
const debug = require("debug");
const serverDebug = debug('StockDatasetAPI:server');
const bodyParser = require("body-parser");
const ErrorExist_1 = require("../errors/ErrorExist");
const ErrorNoUser_1 = require("../errors/ErrorNoUser");
const ErrorTrade_1 = require("../errors/ErrorTrade");
const router = express.Router();
class TradeRoutes {
    constructor() {
        this.insert = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.tradeController.insert(req.body);
                res.sendStatus(201);
            }
            catch (e) {
                if (e instanceof ErrorExist_1.default || e instanceof ErrorTrade_1.default) {
                    res.sendStatus(400);
                }
                else {
                    serverDebug(e);
                    res.sendStatus(500);
                }
            }
        });
        this.findByUserId = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const trades = yield this.tradeController.findByUserId(req.params.id);
                res.json(trades);
            }
            catch (e) {
                if (e instanceof ErrorNoUser_1.default) {
                    res.sendStatus(404);
                }
                else {
                    serverDebug(e);
                    res.sendStatus(500);
                }
            }
        });
        this.get = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const trades = yield this.tradeController.get();
                res.json(trades);
            }
            catch (e) {
                serverDebug(e);
                res.sendStatus(500);
            }
        });
        this.tradeController = new trades_1.TradeController();
    }
    buildRoutes() {
        router.post('', bodyParser.json(), this.insert);
        router.get('', this.get);
        router.get('/users/:id', this.findByUserId);
        return router;
    }
}
exports.TradeRoutes = TradeRoutes;
