import * as express from 'express';
import { TradeController } from '../controllers/trades'
import { Response, Request } from 'express-serve-static-core';
import * as debug from 'debug';
const serverDebug = debug('StockDatasetAPI:server');
import * as bodyParser from 'body-parser';
import ErrorExists from '../errors/ErrorExist';
import ErrorNoUser from '../errors/ErrorNoUser';
import ErrorTrade from '../errors/ErrorTrade';

const router = express.Router();

export class TradeRoutes {

    private tradeController: TradeController;

    constructor() {
        this.tradeController = new TradeController();
    }

    public buildRoutes(): express.Router {
        router.post('', bodyParser.json(), this.insert);
        router.get('', this.get);
        router.get('/users/:id', this.findByUserId);
        return router;
    }

    public insert = async (req: Request, res: Response): Promise<void> => {
        try {
            await this.tradeController.insert(req.body);
            res.sendStatus(201);
        } catch (e) {
            if (e instanceof ErrorExists || e instanceof ErrorTrade) {
                res.sendStatus(400);
            } else {
                serverDebug(e);
                res.sendStatus(500);
            }
        }
    }

    public findByUserId = async (req: Request, res: Response): Promise<void> => {
        try {
            const trades = await this.tradeController.findByUserId(req.params.id);
            res.json(trades);
        } catch (e) {
            if (e instanceof ErrorNoUser) {
                res.sendStatus(404);
            } else {
                serverDebug(e);
                res.sendStatus(500);
            }
        }
    }

    public get = async (req: Request, res: Response): Promise<void> => {
        try {
            const trades = await this.tradeController.get();
            res.json(trades);
        } catch (e) {
            serverDebug(e);
            res.sendStatus(500);
        }
    }
}
