import * as express from 'express';
import { TradeController } from '../controllers/trades'
import { Response, Request } from 'express-serve-static-core';
import * as debug from 'debug';
const serverDebug = debug('StockDatasetAPI:server');

const router = express.Router();

export class EraseRoutes {

    private tradeController: TradeController;

    constructor(){
        this.tradeController = new TradeController();
    }

    public buildRoutes(): express.Router {
        router.delete('/trades',  this.deleteTrades );
        return router;
    }

    public deleteTrades = async (req: Request, res: Response): Promise<void> => {
        try {
            await this.tradeController.deleteAllTrades();
            res.sendStatus(200);
        } catch(e) {
            serverDebug(e);
            res.sendStatus(500);
        }
    }
}
