import { DbService } from '../db/dbService';
import ErrorExists from '../errors/ErrorExist';
import ErrorNoUser from '../errors/ErrorNoUser';
import ErrorTrade from '../errors/ErrorTrade';

export class TradeController { 

    constructor(){}

    public deleteAllTrades = async (): Promise<void> => {
        await DbService.deleteTrades();
    }

    public insert = async (trade: ITrade): Promise<void> => {
        trade.price = parseFloat(trade.price.toFixed(2));
        this.validateTrade(trade);
        const exist = await DbService.existTradeById(trade.id);
        if(exist) {
            throw new ErrorExists('Trade Exists');
        } else {
            await DbService.insertTrade(trade);
        }
    }

    public findByUserId = async (id: number): Promise<ITrade[]> => {
        const exist = await DbService.existUserById(id);
        if(!exist) {
            throw new ErrorNoUser(`User ${id} doesn't Exists`);
        } else {
            return await DbService.findByUserId(id);
        }
    }

    public get = async (): Promise<ITrade[]> => {
        return await DbService.getTrades();
    }

    private validateTrade = (trade: ITrade) => {
        if(trade.price < 130.42 || trade.price > 195.65) {
            throw new ErrorTrade('Price not valid')
        } else if( trade.shares < 10 || trade.shares > 30) {
            throw new ErrorTrade('Share not between range')
        }
    }
}
