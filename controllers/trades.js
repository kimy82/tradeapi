"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dbService_1 = require("../db/dbService");
const ErrorExist_1 = require("../errors/ErrorExist");
const ErrorNoUser_1 = require("../errors/ErrorNoUser");
const ErrorTrade_1 = require("../errors/ErrorTrade");
class TradeController {
    constructor() {
        this.deleteAllTrades = () => __awaiter(this, void 0, void 0, function* () {
            yield dbService_1.DbService.deleteTrades();
        });
        this.insert = (trade) => __awaiter(this, void 0, void 0, function* () {
            trade.price = parseFloat(trade.price.toFixed(2));
            this.validateTrade(trade);
            const exist = yield dbService_1.DbService.existTradeById(trade.id);
            if (exist) {
                throw new ErrorExist_1.default('Trade Exists');
            }
            else {
                yield dbService_1.DbService.insertTrade(trade);
            }
        });
        this.findByUserId = (id) => __awaiter(this, void 0, void 0, function* () {
            const exist = yield dbService_1.DbService.existUserById(id);
            if (!exist) {
                throw new ErrorNoUser_1.default(`User ${id} doesn't Exists`);
            }
            else {
                return yield dbService_1.DbService.findByUserId(id);
            }
        });
        this.get = () => __awaiter(this, void 0, void 0, function* () {
            return yield dbService_1.DbService.getTrades();
        });
        this.validateTrade = (trade) => {
            if (trade.price < 130.42 || trade.price > 195.65) {
                throw new ErrorTrade_1.default('Price not valid');
            }
            else if (trade.shares < 10 || trade.shares > 30) {
                throw new ErrorTrade_1.default('Share not between range');
            }
        };
    }
}
exports.TradeController = TradeController;
